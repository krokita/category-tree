## Install

Clone repository and run

``` bash
$ composer install
```

## Usage

``` php
$categoriesFile = __DIR__ . '/files/categories.json';
$categoriesStructureFile = __DIR__ . '/files/structure.json';

$categorySource = new CategoryTree\CategorySource\JsonCategorySource(
    $categoriesFile,
    new \CategoryTree\Factory\CategoryFactory(
        CategoryTree\Enum\LanguageCodeEnum::PL
    )
);

$categoryStructure = new \CategoryTree\CategoryStructure\JsonCategoryStructure(
    $categoriesStructureFile,
    new \CategoryTree\Factory\CategoryStructureFactory()
);

$categoryTree = new CategoryTree\CategoryTreeBuilder(
    $categorySource,
    $categoryStructure,
    new \CategoryTree\MergeStrategy\CategoryIdMergeStrategy()
);

echo $categoryTree->build();

```

## Testing

In root directory, copy `phpunit.xml.dist` file and rename to `phpunit.xml`. Run tests via command

``` bash
$ ./vendor/bin/phpunit
```
