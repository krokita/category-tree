<?php

require_once __DIR__ . '/../vendor/autoload.php';

$categoriesFile = __DIR__ . '/files/categories.json';
$categoriesStructureFile = __DIR__ . '/files/structure.json';

$categorySource = new CategoryTree\CategorySource\JsonCategorySource(
    $categoriesFile,
    new \CategoryTree\Factory\CategoryFactory(
        CategoryTree\Enum\LanguageCodeEnum::PL
    )
);

$categoryStructure = new \CategoryTree\CategoryStructure\JsonCategoryStructure(
    $categoriesStructureFile,
    new \CategoryTree\Factory\CategoryStructureFactory()
);

$categoryTree = new CategoryTree\CategoryTreeBuilder(
    $categorySource,
    $categoryStructure,
    new \CategoryTree\MergeStrategy\CategoryIdMergeStrategy()
);

echo $categoryTree->build();

