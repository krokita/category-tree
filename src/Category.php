<?php

declare(strict_types=1);

namespace CategoryTree;

class Category
{
    /**
     * @var string 
     */
    private $categoryId;
    
    /**
     * @var string|null 
     */
    private $name;
    
    public function __construct(string $categoryId, ?string $name)
    {
        $this->categoryId = $categoryId;
        $this->name = $name;
    }

    public function getCategoryId(): string
    {
        return $this->categoryId;
    }

    public function getName(): ?string
    {
        return $this->name;
    }
}
