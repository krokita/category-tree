<?php

declare(strict_types=1);

namespace CategoryTree\CategorySource;

use CategoryTree\CategorySourceInterface;
use CategoryTree\Factory\CategoryFactory;
use CategoryTree\Utils\JsonUtils;
use CategoryTree\Utils\FileUtils;

class JsonCategorySource implements CategorySourceInterface
{
    /**
     * @var string
     */
    private $file;
    
    /**
     * @var CategoryFactory
     */
    private $categoryFactory;
    
    public function __construct(string $file, CategoryFactory $categoryFactory)
    {
        $this->file = $file;
        $this->categoryFactory = $categoryFactory;
    }
    
    public function getCategories(): array
    {
        $categories = [];
        $categoriesFromFile = JsonUtils::parseJsonContent(
            FileUtils::getContentFile($this->file)
        );
        
        foreach ($categoriesFromFile as $category) {
            $categories[] = $this->categoryFactory->create($category);
        }
        
        return $categories;
    }
}
