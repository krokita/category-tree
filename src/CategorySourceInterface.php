<?php

declare(strict_types=1);

namespace CategoryTree;

interface CategorySourceInterface
{
    /**
     * Receive categories
     * 
     * @return array
     */
    public function getCategories(): array;
}
