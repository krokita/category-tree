<?php

declare(strict_types=1);

namespace CategoryTree\CategoryStructure;

use CategoryTree\CategoryStructureInterface;
use CategoryTree\Factory\CategoryStructureFactory;
use CategoryTree\Utils\JsonUtils;
use CategoryTree\Utils\FileUtils;

class JsonCategoryStructure implements CategoryStructureInterface
{
    /**
     * @var string 
     */
    private $file;
    
    /**
     * @var CategoryStructureFactory 
     */
    private $categoryStructureFactory;
    
    public function __construct(
        string $file,
        CategoryStructureFactory $categoryStructureFactory
    ) {
        $this->file = $file;
        $this->categoryStructureFactory = $categoryStructureFactory;
    }
    
    public function getStructure(): array
    {
        $structure = [];
        $structureFromFile = JsonUtils::parseJsonContent(
            FileUtils::getContentFile($this->file)
        );
        
        foreach ($structureFromFile as $element) {
            $structure[] = $this->categoryStructureFactory->create($element);
        }
        
        return $structure;
    }
}
