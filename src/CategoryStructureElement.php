<?php

declare(strict_types=1);

namespace CategoryTree;

class CategoryStructureElement
{
    /**
     * @var int 
     */
    private $id;
    
    /**
     * @var array|CategoryStructureElement[] 
     */
    private $children;
    
    public function __construct(int $id, array $children)
    {
        $this->id = $id;
        $this->children = $children;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getChildren(): array
    {
        return $this->children;
    }
    
    public function hasChildren(): bool
    {
        return !empty($this->children);
    }
}
