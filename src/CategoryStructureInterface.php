<?php

declare(strict_types=1);

namespace CategoryTree;

use CategoryTree\CategoryStructureElement;

interface CategoryStructureInterface
{
    /**
     * Receive category tree structure
     * 
     * @return array|CategoryStructureElement[]
     */
    public function getStructure(): array;
}
