<?php

declare(strict_types=1);

namespace CategoryTree;

use CategoryTree\CategorySourceInterface;
use CategoryTree\CategoryStructureInterface;
use CategoryTree\MergeStrategyInterface;

class CategoryTreeBuilder
{
    /**
     * @var CategorySourceInterface
     */
    private $categorySource;
    
    /**
     * @var CategoryStructureInterface 
     */
    private $categoryStructure;
    
    /**
     * @var MergeStrategyInterface
     */
    private $mergeStrategy;
    
    public function __construct(
        CategorySourceInterface $categorySource,
        CategoryStructureInterface $categoryStructure,
        MergeStrategyInterface $mergeStrategy
    ) {
        $this->categorySource = $categorySource;
        $this->categoryStructure = $categoryStructure;
        $this->mergeStrategy = $mergeStrategy;
    }
    
    /**
     * Build category tree
     * 
     * @param string $languageCode
     * @return string
     */
    public function build(): string
    {
        $leafs = $this->mergeStrategy->merge(
            $this->categorySource->getCategories(),
            $this->categoryStructure->getStructure()
        );
        
        // here... maybe some another output for xml
        
        return \json_encode($leafs);
    }
}
