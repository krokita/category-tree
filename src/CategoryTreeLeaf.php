<?php

declare(strict_types=1);

namespace CategoryTree;

class CategoryTreeLeaf implements \JsonSerializable
{
    /**
     * @var int 
     */
    private $id;
    
    /**
     * @var string
     */
    private $name;
    
    /**
     * @var array|CategoryTreeLeaf[] 
     */
    private $children;
    
    public function __construct(int $id, string $name, array $children)
    {
        $this->id = $id;
        $this->name = $name;
        $this->children = $children;
    }

    public function getId(): int
    {
        return $this->id;
    }
    
    public function getName(): string
    {
        return $this->name;
    }

    public function getChildren(): array
    {
        return $this->children;
    }
    
    public function hasChildren(): bool
    {
        return !empty($this->children);
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'children' => $this->children
        ];
    }
}
