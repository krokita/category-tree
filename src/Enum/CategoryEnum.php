<?php

declare(strict_types=1);

namespace CategoryTree\Enum;

class CategoryEnum
{
    // Category Id field
    public const FIELD_CATEGORY_ID = 'category_id';
    
    // Category Translations field
    public const FIELD_TRANSLATIONS = 'translations';
    
    // Category Name field
    public const FIELD_NAME = 'name';
}
