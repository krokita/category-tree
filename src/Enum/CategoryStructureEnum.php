<?php

declare(strict_types=1);

namespace CategoryTree\Enum;

class CategoryStructureEnum
{
    // Id field
    public const FIELD_ID = 'id';
    
    // Children field
    public const FIELD_CHILDREN = 'children';
}
