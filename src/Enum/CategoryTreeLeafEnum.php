<?php

declare(strict_types=1);

namespace CategoryTree\Enum;

class CategoryTreeLeafEnum
{
    // Id field
    public const FIELD_ID = 'id';
    
    // Category Name field
    public const FIELD_NAME = 'name';
    
    // Children field
    public const FIELD_CHILDREN = 'children';
}
