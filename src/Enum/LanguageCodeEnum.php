<?php

declare(strict_types=1);

namespace CategoryTree\Enum;

class LanguageCodeEnum
{
    // Polish language
    public const PL = 'pl_PL';
    
    // English American language
    public const EN_US = 'en_US';
}
