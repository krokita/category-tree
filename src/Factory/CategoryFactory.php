<?php

declare(strict_types=1);

namespace CategoryTree\Factory;

use CategoryTree\Category;
use CategoryTree\Enum\CategoryEnum;

class CategoryFactory
{
    /**
     * @var string 
     */
    private $languageCode;
    
    public function __construct(string $languageCode)
    {
        $this->languageCode = $languageCode;
    }
    
    public function create(array $category): Category
    {
        if (!isset($category[CategoryEnum::FIELD_TRANSLATIONS])) {
            throw new \Exception('Translations field is not set');
        }
        
        $translations = $category[CategoryEnum::FIELD_TRANSLATIONS];
        
        return new Category(
            $category[CategoryEnum::FIELD_CATEGORY_ID],
            $this->getCategoryName($translations)
        );
    }
    
    private function getCategoryName(array $translations): ?string
    {
        if (!isset($translations[$this->languageCode])) {
            throw new \Exception(
                \sprintf('Translation for "%s" is not set', $this->languageCode)
            );
        }
        
        $translation = $translations[$this->languageCode];
        
        return isset($translation[CategoryEnum::FIELD_NAME]) ?
            $translation[CategoryEnum::FIELD_NAME] : 
            null;
    }
}
