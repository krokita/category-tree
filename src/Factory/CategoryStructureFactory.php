<?php

declare(strict_types=1);

namespace CategoryTree\Factory;

use CategoryTree\CategoryStructureElement;
use CategoryTree\Enum\CategoryStructureEnum;

class CategoryStructureFactory
{
    public function create(array $leaf): CategoryStructureElement
    {
        $children = $leaf[CategoryStructureEnum::FIELD_CHILDREN];
        $hasChildren = !empty($children);
        $childrenCategoryElements = [];
        
        if ($hasChildren) {
            foreach ($children as $child) {
                $childrenCategoryElements[] = $this->create($child);
            }
        }
        
        return new CategoryStructureElement(
            $leaf[CategoryStructureEnum::FIELD_ID],
            $childrenCategoryElements
        );
    }
}
