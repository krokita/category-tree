<?php

declare(strict_types=1);

namespace CategoryTree\MergeStrategy;

use CategoryTree\MergeStrategyInterface;
use CategoryTree\CategoryTreeLeaf;
use CategoryTree\CategoryStructureElement;

class CategoryIdMergeStrategy implements MergeStrategyInterface
{
    public function merge(array $categories, array $structure): array
    {
        $indexedCategories = $this->buildIndexedCategories($categories);
        $leafs = [];
        
        foreach ($structure as $element) {
            $leafs[] = $this->buildCategoryLeafs(
                $indexedCategories,
                $element
            );
        }
        
        return $leafs;
    }
    
    private function buildIndexedCategories(array $categories): array
    {
        $indexedCategories = [];
        
        foreach ($categories as $category) {
            $indexedCategories[$category->getCategoryId()] = $category;
        }
        
        return $indexedCategories;
    }
    
    private function buildCategoryLeafs(array $categories, CategoryStructureElement $element): CategoryTreeLeaf
    {
        $leafChildren = [];
        
        if ($element->hasChildren()) {
            foreach ($element->getChildren() as $child) {
                $leafChildren[] = $this->buildCategoryLeafs($categories, $child);
            }
        }
        
        $category = isset($categories[$element->getId()]) ? 
            $categories[$element->getId()] : null;
        $categoryName = $category ? $category->getName() : '';
        
        return new CategoryTreeLeaf(
            $element->getId(),
            $categoryName,
            $leafChildren
        );
    }
}
