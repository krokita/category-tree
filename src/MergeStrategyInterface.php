<?php

declare(strict_types=1);

namespace CategoryTree;

interface MergeStrategyInterface
{
    public function merge(array $categories, array $structure): array;
}
