<?php

declare(strict_types=1);

namespace CategoryTree\Utils;

class FileUtils
{
    /**
     * Receive file content
     * @param string $file
     * @return string
     * @throws \RuntimeException
     */
    public static function getContentFile(string $file): string
    {
        if (!\file_exists($file)) {
            throw new \RuntimeException(
                \sprintf('File with path %s not exists', $file)
            );
        }
        
        return \file_get_contents($file);
    }
}
