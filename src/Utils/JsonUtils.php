<?php

declare(strict_types=1);

namespace CategoryTree\Utils;

class JsonUtils
{
    /**
     * Receive parsed json content
     * @param string $content
     * @return array
     * @throws \Exception
     */
    public static function parseJsonContent(string $content): array
    {
        if (empty($content)) {
            throw new \Exception('Empty content');
        }
        
        return \json_decode($content, true);
    }
}
