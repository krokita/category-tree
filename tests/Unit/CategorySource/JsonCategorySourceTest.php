<?php

declare(strict_types=1);

namespace CategoryTree\Tests\Unit\CategorySource;

use PHPUnit\Framework\TestCase;
use CategoryTree\CategorySource\JsonCategorySource;
use CategoryTree\Factory\CategoryFactory;
use CategoryTree\Enum\LanguageCodeEnum;

class JsonCategorySourceTest extends TestCase
{
    public function testShouldGetCategoriesFromJsonFile()
    {
        $file = __DIR__ . '/../../Resources/test_categories.json';
        $factory = new CategoryFactory(LanguageCodeEnum::PL);
        $source = new JsonCategorySource($file, $factory);
        
        $categories = $source->getCategories();
        
        $this->assertCount(2, $categories);
    }
}
