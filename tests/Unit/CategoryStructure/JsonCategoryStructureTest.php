<?php

declare(strict_types=1);

namespace CategoryTree\Tests\Unit\CategoryStructure;

use PHPUnit\Framework\TestCase;
use CategoryTree\CategoryStructure\JsonCategoryStructure;
use CategoryTree\Factory\CategoryStructureFactory;

class JsonCategoryStructureTest extends TestCase
{
    public function testShouldGetStructureFromJsonFile()
    {
        $file = __DIR__ . '/../../Resources/test_structure.json';
        $factory = new CategoryStructureFactory();
        $structure = new JsonCategoryStructure($file, $factory);
        
        $structureElements = $structure->getStructure();
        $firstElement = \current($structureElements);
        
        $this->assertCount(1, $structureElements);
        $this->assertSame(1, $firstElement->getId());
    }
}
