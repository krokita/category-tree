<?php

declare(strict_types=1);

namespace CategoryTree\Tests\Unit;

use PHPUnit\Framework\TestCase;
use CategoryTree\CategoryTreeBuilder;
use CategoryTree\CategorySource\JsonCategorySource;
use CategoryTree\CategoryStructure\JsonCategoryStructure;
use CategoryTree\MergeStrategy\CategoryIdMergeStrategy;
use CategoryTree\Enum\LanguageCodeEnum;
use CategoryTree\Factory\CategoryFactory;
use CategoryTree\Factory\CategoryStructureFactory;
use CategoryTree\Enum\CategoryTreeLeafEnum;

class CategoryTreeBuilderTest extends TestCase
{
    public function testShouldBuildCategoryTree()
    {
        $builder = new CategoryTreeBuilder(
            $this->getCategorySource(),
            $this->getCategoryStructure(),
            new CategoryIdMergeStrategy()
        );
        
        $categoryTree = $builder->build();
        
        $this->assertSame($this->getExpectedCategoryTree(), $categoryTree);
    }
    
    private function getCategorySource(): JsonCategorySource
    {
        $file = __DIR__ . '/../Resources/test_categories.json';
        $factory = new CategoryFactory(LanguageCodeEnum::PL);
        
        return new JsonCategorySource($file, $factory);
    }
    
    private function getCategoryStructure(): JsonCategoryStructure
    {
        $file = __DIR__ . '/../Resources/test_structure.json';
        $factory = new CategoryStructureFactory();
        
        return new JsonCategoryStructure($file, $factory);
    }
    
    private function getExpectedCategoryTree(): string
    {
        $categoryTree = [
            [
                CategoryTreeLeafEnum::FIELD_ID => 1,
                CategoryTreeLeafEnum::FIELD_NAME => 'Category #1',
                CategoryTreeLeafEnum::FIELD_CHILDREN => [
                    [
                        CategoryTreeLeafEnum::FIELD_ID => 2,
                        CategoryTreeLeafEnum::FIELD_NAME => 'Category #2',
                        CategoryTreeLeafEnum::FIELD_CHILDREN => [],
                    ],
                ],
            ]
        ];
        
        return \json_encode($categoryTree);
    }
}
