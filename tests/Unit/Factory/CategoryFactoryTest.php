<?php

namespace CategoryTree\Tests\Unit\Factory;

use PHPUnit\Framework\TestCase;
use CategoryTree\Factory\CategoryFactory;
use CategoryTree\Enum\LanguageCodeEnum;
use CategoryTree\Enum\CategoryEnum;

class CategoryFactoryTest extends TestCase
{
    /**
     * @dataProvider provideData
     */
    public function testShouldCreateCategory(
        string $languageCode,
        string $categoryId,
        string $categoryName,
        array $categoryAsArray
    ): void {
        $factory = new CategoryFactory($languageCode);
        
        $category = $factory->create($categoryAsArray);
        
        $this->assertSame($categoryId, $category->getCategoryId());
        $this->assertSame($categoryName, $category->getName());
    }
    
    public function testShouldThrowExceptionWhenTranslationIsNotSet(): void 
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Translation for "pl_PL" is not set');
        
        $factory = new CategoryFactory(LanguageCodeEnum::PL);
        
        $factory->create($this->getCategoryDataWithOnlyEnglishTranslation());
    }
    
    public function provideData(): array
    {
        return [
            [
                LanguageCodeEnum::PL,
                '1',
                'Kategoria #1',
                [
                    CategoryEnum::FIELD_CATEGORY_ID => '1',
                    CategoryEnum::FIELD_TRANSLATIONS => [
                        LanguageCodeEnum::PL => [
                            CategoryEnum::FIELD_NAME => 'Kategoria #1',
                        ],
                    ],
                ],
            ],
            [
                LanguageCodeEnum::EN_US,
                '2',
                'Category #2',
                [
                    CategoryEnum::FIELD_CATEGORY_ID => '2',
                    CategoryEnum::FIELD_TRANSLATIONS => [
                        LanguageCodeEnum::PL => [
                            CategoryEnum::FIELD_NAME => 'Kategoria #2',
                        ],
                        LanguageCodeEnum::EN_US => [
                            CategoryEnum::FIELD_NAME => 'Category #2',
                        ],
                    ],
                ],
            ],
        ];
    }
    
    private function getCategoryDataWithOnlyEnglishTranslation(): array
    {
        return [
            CategoryEnum::FIELD_CATEGORY_ID => '3',
            CategoryEnum::FIELD_TRANSLATIONS => [
                LanguageCodeEnum::EN_US => [
                    CategoryEnum::FIELD_NAME => 'Category #3',
                ],
            ],
        ];
    }
}
