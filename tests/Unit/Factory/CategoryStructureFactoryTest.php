<?php

namespace CategoryTree\Tests\Unit\Factory;

use PHPUnit\Framework\TestCase;
use CategoryTree\Factory\CategoryStructureFactory;
use CategoryTree\Enum\CategoryStructureEnum;

class CategoryStructureFactoryTest extends TestCase
{
    public function testShouldCreateSimpleCategoryStructure(): void
    {
        $factory = new CategoryStructureFactory();
        
        $categoryStructure = $factory->create($this->getSimpleLeaf());
        
        $this->assertSame(1, $categoryStructure->getId());
        $this->assertFalse($categoryStructure->hasChildren());
    }
    
    public function testShouldCreateExtendedCategoryStructure(): void
    {
        $factory = new CategoryStructureFactory();
        
        $categoryStructure = $factory->create($this->getExtendedLeaf());
        $children = $categoryStructure->getChildren();
        $firstChild = \current($children);
        
        $this->assertSame(2, $categoryStructure->getId());
        $this->assertTrue($categoryStructure->hasChildren());
        $this->assertSame(3, $firstChild->getId());
        $this->assertFalse($firstChild->hasChildren());
    }
    
    private function getSimpleLeaf(): array
    {
        return [
            CategoryStructureEnum::FIELD_ID => 1,
            CategoryStructureEnum::FIELD_CHILDREN => [],
        ];
    }
    
    
    public function getExtendedLeaf(): array
    {
        return [
            CategoryStructureEnum::FIELD_ID => 2,
            CategoryStructureEnum::FIELD_CHILDREN => [
                [
                    CategoryStructureEnum::FIELD_ID => 3,
                    CategoryStructureEnum::FIELD_CHILDREN => [],
                ],
                [
                    CategoryStructureEnum::FIELD_ID => 4,
                    CategoryStructureEnum::FIELD_CHILDREN => [],
                ],
            ],
        ];
    }
}
