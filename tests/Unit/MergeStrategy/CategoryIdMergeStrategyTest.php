<?php

namespace CategoryTree\Unit\MergeStrategy;

use PHPUnit\Framework\TestCase;
use CategoryTree\MergeStrategy\CategoryIdMergeStrategy;
use CategoryTree\Category;
use CategoryTree\CategoryStructureElement;
use CategoryTree\CategoryTreeLeaf;

class CategoryIdMergeStrategyTest extends TestCase
{
    public function testShouldMergeStructureWithCategoryById(): void 
    {
        $categories = [
            new Category(1, 'Category #1'),
            new Category(2, 'Category #2'),
        ];
        $structure = [
            new CategoryStructureElement(1, []),
            new CategoryStructureElement(2, []),
        ];
        $expectedLeafs = [
            new CategoryTreeLeaf(1, 'Category #1', []),
            new CategoryTreeLeaf(2, 'Category #2', []),
        ];
        $mergeStrategy = new CategoryIdMergeStrategy();
        
        $leafs = $mergeStrategy->merge($categories, $structure);
        
        $this->assertEquals($expectedLeafs, $leafs);
    }
}
